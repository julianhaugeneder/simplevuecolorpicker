![alt text](https://miro.medium.com/max/3000/1*39KJ_80ez8YHxCZ-hbfZVw.png "VueSimpleColorPicker")
# Simple Vue Colorpicker  
This project is a global colorpicker for your Vue.js application. In the colorpicker you can select certain colors, which then determine the look of the application. You can then store these colors either locally as a cookie or to an API to store them in a database. For this article I decided to use cookies, but the code is actually the same for APIs.

## Medium Article / Documentation
https://medium.com/@JulianHaugeneder/change-your-sites-color-scheme-with-a-awesome-colorpicker-in-vue-js-fad968e352f4

## Online Demo
https://vuecolorpicker.haugeneder.dev/

## Run Local
```
npm i
vue-cli-service serve
```

## License

[ISC License ](https://choosealicense.com/licenses/isc/)

**Copyright (c) 2019, Julian Haugeneder** 

Permission to use, copy, modify, and/or distribute this software for any  
purpose with or without fee is hereby granted, provided that the above  
copyright notice and this permission notice appear in all copies.  

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES  
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF  
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR  
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES  
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN  
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF  
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
